#ifndef BitVector_H_
#define BitVector_H_
#define BitVectorDebug 1
#define BitVectorGarbageCollectionBufferSize 128

#include <stddef.h>
#include <stdio.h>
#include <stdint-gcc.h>

#define _BitVector_offset(target,of,ps) (of)=((~0x07)&(target))>>3;(ps)=0x07&(target);

struct BitVector{
    size_t len;
    char gc_ctl;
    #if BitVectorDebug == 1
        char removed;
    #endif // BitVectorDebug
    unsigned char *arr;
};

struct BitVector BitVectorGarbageCollectionBuffer[BitVectorGarbageCollectionBufferSize];
struct BitVector *BitVectorGarbageCollectionBufferPtr;

unsigned char _BitVector_multiplexer_3to8(unsigned char t);

unsigned char _BitVector_render_right(unsigned char t,unsigned char shift);

unsigned char _BitVector_render_left(unsigned char t,unsigned char shift);


struct BitVector * _BitVector_init(char gc_ctl);

int _BitVector_remove(struct BitVector *b);

int _BitVector_garbage_collection();

int BitVector_super_init();

struct BitVector * BitVector_init(size_t len,char gc_ctl);

struct BitVector * BitVector_init_fp(size_t len,char gc_ctl,FILE *fp,size_t bytes);

struct BitVector * BitVector_init_string(size_t len,char gc_ctl,char *buff,size_t bytes);


char BitVector_at_bit(struct BitVector *b,size_t p);

char BitVector_at_char(struct BitVector *b,size_t p);

int32_t BitVector_at_int32t(struct BitVector *b,size_t p);

int BitVector_assign_bit(struct BitVector *b,size_t p,char t);

int BitVector_assign_char(struct BitVector *b,size_t p,char t);

int BitVector_assign_int32t(struct BitVector *b,size_t p,int32_t t);

int BitVector_reverse_bit(struct BitVector *b,size_t p);

int BitVector_reverse(struct BitVector *b,size_t s,size_t e);



struct BitVector * BitVector_copy(struct BitVector *b);

struct BitVector * BitVector_sub(struct BitVector *b,size_t s,size_t e);

struct BitVector * BitVector_append(struct BitVector *b,struct BitVector *bv);

int BitVector_find(struct BitVector *b,struct BitVector *bv);

int BitVector_cmp(struct BitVector *b,struct BitVector *bv);


struct BitVector * BitVector_insert(struct BitVector *b,struct BitVector *bv,size_t);


#endif // BitVector_H_
