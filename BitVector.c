#include "BitVector.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
struct BitVector{
    size_t len;
    char gc_ctl;
    #if BitVectorDebug == 1
        char removed;
    #endif // BitVectorDebug
    char *arr;
};
*/

inline unsigned char _BitVector_multiplexer_3to8(unsigned char t){
    const static unsigned char ans_table[9]={0,1,2,4,8,16,32,64,128};
    return ans_table[t];
}

inline unsigned char _BitVector_render_right(unsigned char t,unsigned char shift){
    const static unsigned char ans_table[9]={0,1,2,4,8,16,32,65};
    return ans_table[t];
}

inline unsigned char _BitVector_render_left(unsigned char t,unsigned char shift){
    const static unsigned char ans_table[8]={0,1,2,4,8,16,32,65};
    return ans_table[t];
}

struct BitVector * _BitVector_init(char gc_ctl){
    struct BitVector *b = (struct BitVector*)malloc(sizeof(struct BitVector));
    b->gc_ctl=gc_ctl;
    #if BitVectorDebug == 1
        b->removed=0;
    #endif // BitVectorDebug
    BitVectorGarbageCollectionBufferPtr=b;
    BitVectorGarbageCollectionBufferPtr++;
    return b;
}

int _BitVector_remove(struct BitVector *b){
    #if BitVectorDebug == 1
        b->removed=1;
    #elif BitVectorDebug == 0
        free(arr);
        free(b);
    #endif // BitVectorDebug
    return 1;
}

int _BitVector_garbage_collection() {
    size_t amount = BitVectorGarbageCollectionBufferPtr - &(BitVectorGarbageCollectionBuffer[0]);
    size_t piont = (BitVectorGarbageCollectionBufferSize/4);
    if(amount >  (BitVectorGarbageCollectionBufferSize/4)*3){
        for(size_t i;i<piont;++i){
            _BitVector_remove(&(BitVectorGarbageCollectionBuffer[i]));
        }
        for(size_t i;&(BitVectorGarbageCollectionBuffer[piont+i])!=BitVectorGarbageCollectionBufferPtr;++i){
            BitVectorGarbageCollectionBuffer[i]=BitVectorGarbageCollectionBuffer[piont+i];
        }
    }
    return amount-piont;
}

int BitVector_super_init() {
    BitVectorGarbageCollectionBufferPtr = &(BitVectorGarbageCollectionBuffer[0]);
    return 1;
}


struct BitVector * BitVector_init(size_t len,char gc_ctl){
    struct BitVector *b = _BitVector_init(gc_ctl);
    b->arr = (char*)malloc(sizeof(char)*len/8);
    if(b->arr == NULL){
        perror("BitVector_init::malloc b::arr fail.\n");
    }else{
        memset(b->arr,0,len/8);
    }
    b->len=len;
    return b;
}

struct BitVector * BitVector_init_fp(size_t len,char gc_ctl,FILE *fp,size_t bytes);

struct BitVector * BitVector_init_string(size_t len,char gc_ctl,char *buff,size_t bytes);


char BitVector_at_bit(struct BitVector *b,size_t p){
    size_t of,ps;
    _BitVector_offset(p,of,ps);
    b->arr[of]=b->arr[of] & _BitVector_multiplexer_3to8(ps);
}

char BitVector_at_char(struct BitVector *b,size_t p){
    size_t sof,sps,dof,dps;
    _BitVector_offset(p,sof,sps);
    if(sps == 0){
        return b->arr[sof];
    }else{
        char ans;
        _BitVector_offset(p+8,dof,dps);
        ans=_BitVector_render_right(b->arr[sof],0)+_BitVector_render_right(b->arr[dof],0);
        return ans;
    }
}

int32_t BitVector_at_int32t(struct BitVector *b,size_t p);

int BitVector_assign_bit(struct BitVector *b,size_t p,char t);

int BitVector_assign_char(struct BitVector *b,size_t p,char t);

int BitVector_assign_int32t(struct BitVector *b,size_t p,int32_t t);

int BitVector_reverse_bit(struct BitVector *b,size_t p);

int BitVector_reverse(struct BitVector *b,size_t s,size_t e);



struct BitVector * BitVector_copy(struct BitVector *b);

struct BitVector * BitVector_sub(struct BitVector *b,size_t s,size_t e);

struct BitVector * BitVector_append(struct BitVector *b,struct BitVector *bv);


int BitVector_find(struct BitVector *b,struct BitVector *bv);

int BitVector_cmp(struct BitVector *b,struct BitVector *bv);


struct BitVector * BitVector_insert(struct BitVector *b,struct BitVector *bv,size_t);

int main(){ //unit test only
    size_t of,ps;
    unsigned char c;
    _BitVector_offset(127,of,ps);
    c=_BitVector_multiplexer_3to8(ps);
    printf("of:%ld ps:%ld multiplexer:%d\n",of,ps,c);


}
