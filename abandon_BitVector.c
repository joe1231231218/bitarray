#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint-gcc.h>

#define BitVectorDebug 1
#define BitVectorRangeCheckOpen 1
#define BitVectorGarbageCollectionBufferSize 128

#if BitVectorDebug == 1
    #define BitVectorRangeCheckOpen 1
    #define BitVectorDebugRemovedObj(b) if((b)->removed != 0){printf("(BitVector)%p removed but access.\n");}
    //struct BitVector *b
#else
    #define BitVectorDebugRemovedObj(b) ;
#endif // BitVectorDebug

#define _BitVector_at_bit(b,p) ((b)->arr[(p)] == 0 ? 0 : 1) //struct BitVector *b,size_t p
#define _BitVector_assign_bit(b,p,t) ((b)->arr[(p)] = (t) == 0 ? 0 : 1) //struct BitVector *b,size_t p,char t
#define BitVector_size(b) (b)->len //struct BitVector *b

#if BitVectorRangeCheckOpen == 1
    #define BitVectorRangeCheck(b,p,err_dst) BitVectorDebugRemovedObj(b);if((b)->len <= p){printf("%s (BitVector)%p position:%d\n",(err_dst),(b),(p));}
    //struct BitVector *b,size_t p,char* err_dst
#else
    #define BitVectorRangeCheck(b,p,err_dst) ;
#endif // BitVectorRangeCheckOpen

struct BitVector {
    size_t len;
    unsigned char gc_ctl;
#if BitVectorDebug == 1
    unsigned char removed;
#endif // BitVectorDebug
    unsigned char *arr;
};

struct BitVector *BitVectorGarbageCollectionBuffer[BitVectorGarbageCollectionBufferSize];
struct BitVector **BitVectorGarbageCollectionBufferPtr;

char *BitVectorExportBufferPtr;

int BitVector_assign_char(struct BitVector *,size_t,char);

struct BitVector * _BitVector_init(size_t len,char gc_ctl) {
    struct BitVector *b = (struct BitVector*)malloc(sizeof(struct BitVector));
    b->arr = (unsigned char*)malloc(sizeof(char)*len);
    b->gc_ctl=gc_ctl;
    b->len = len;
#if BitVectorDebug == 1
    memset(b->arr,0,len);
    b->removed=0;
#endif // BitVectorDebug
    *BitVectorGarbageCollectionBufferPtr = b;
    BitVectorGarbageCollectionBufferPtr++;
    return b;
}

int _BitVector_remove(struct BitVector *b) {
#if BitVectorDebug == 1
    b->removed=1;
#elif BitVectorDebug == 0
    free(b->arr);
    free(b);
#endif // BitVectorDebug
    return 1;
}

size_t _BitVector_garbage_collection() {
    size_t rmcn=0; //remove counter//
    struct BitVector **coll,**rmp,**tail,**border;
    coll = &BitVectorGarbageCollectionBuffer[0];
    rmp = &BitVectorGarbageCollectionBuffer[BitVectorGarbageCollectionBufferSize/2];
    border = &BitVectorGarbageCollectionBuffer[(BitVectorGarbageCollectionBufferSize/4)*3];
    tail = &BitVectorGarbageCollectionBuffer[BitVectorGarbageCollectionBufferSize];
    if(BitVectorGarbageCollectionBufferPtr <= border) {
        return 0;
    }
    for(struct BitVector **ptr=coll; ptr!=rmp; ptr++) {
        if((*ptr)->gc_ctl == 0) {
            _BitVector_remove(*ptr);
            ++rmcn;
        } else if((*ptr)->gc_ctl == 1) {
            --((*ptr)->gc_ctl);
        }
    }
    struct BitVector **ptr=coll,**pb=coll;
    for(; pb<=BitVectorGarbageCollectionBufferPtr;) {
        if(pb != NULL) {
            *ptr=*pb;
            ptr++;
            pb++;
        } else {
            pb++;
        }
    }
    BitVectorGarbageCollectionBufferPtr = ptr;
    return rmcn;
}

int BitVector_super_init() {
    BitVectorGarbageCollectionBufferPtr = &(BitVectorGarbageCollectionBuffer[0]);
    BitVectorExportBufferPtr = NULL;
    return 1;
}


struct BitVector * BitVector_init(size_t len,char gc_ctl) {
    struct BitVector *b = _BitVector_init(len,gc_ctl);
    if(b->arr == NULL) {
        printf("BitVector_init::malloc b::arr allocate error (BitVectot)%p.\n",b);
    } else {
        memset(b->arr,0,len);
    }
    b->len=len;
    return b;
}

struct BitVector * BitVector_init_fp(char gc_ctl,FILE *fp) {
    size_t len;
    struct BitVector *b;
    if(fseek(fp,0,SEEK_END) == 0) {
        len = ftell(fp)*8;
    } else {
        perror("BitVector_init_fp::fseek fail.\n");
        return NULL;
    }
    fseek(fp,0,SEEK_END);
    b = _BitVector_init(len,gc_ctl);
    if(b->arr == NULL) {
        printf("BitVector_init_fp::malloc b::arr allocate error (BitVectot)%p.\n",b);
    } else {
        fread(b->arr,sizeof(char),b->len,fp);
    }
    return b;
}

struct BitVector * BitVector_init_string(char gc_ctl,char *buff) {
    size_t len;
    struct BitVector *b;
    len = strlen(buff)*8;
    b = _BitVector_init(len,gc_ctl);
    if(b->arr == NULL) {
        printf("BitVector_init_string::malloc b::arr allocate error (BitVectot)%p.\n",b);
    } else {
        for(size_t i=0;i<(b->len);i+=8){
            BitVector_assign_char(b,i,buff[i]);
        }
    }
    return b;
}

char *BitVector_export_string(struct BitVector *b) {
    size_t p=0,sp=0;
    short cn=7;
    unsigned char c=0,t;
    if(BitVectorExportBufferPtr != NULL) {
        free(BitVectorExportBufferPtr);
    }
    BitVectorExportBufferPtr = (char*)malloc(sizeof(char)*((b->len)/8+1));
    for(p=0; p<((b->len)/8) ; ++p) {
        for(sp=0,t=0;sp<8;sp++){
            t = t | _BitVector_at_bit(b,p*8+sp) << (8-sp);
        }
        BitVectorExportBufferPtr[p] = t;
    }
    BitVectorExportBufferPtr[p] = '\0';
    return BitVectorExportBufferPtr;
}

size_t BitVector_export_fp(struct BitVector *b,FILE *fp) {
    size_t p=0,sp=0;
    short cn=7;
    unsigned char c=0,t;
    for(p=0; p<(b->len); ++p,--cn) {
        t = _BitVector_at_bit(b,p);
        if(t == 1) {
            c+=t<<cn;
        }
        if(cn <= 0) {
            *(BitVectorExportBufferPtr+sp) = c;
            cn=7;
            ++sp;
            fwrite(&c,sizeof(char),1,fp);
            c=0;
        }
    }
    if(cn != 7) {
        fwrite(&c,sizeof(char),1,fp);
    }
    return p%8 == 7 ? p/8 : p/8+1;
}

char *BitVector_export_print(struct BitVector *b) {
    size_t p;
    if(BitVectorExportBufferPtr != NULL) {
        free(BitVectorExportBufferPtr);
    }
    BitVectorExportBufferPtr = (char*)malloc(sizeof(char)*b->len+1);
    for(p=0; p<(b->len); ++p) {
        *(BitVectorExportBufferPtr+p) = _BitVector_at_bit(b,p) == 0 ? '0' : '1';
    }
    *(BitVectorExportBufferPtr+p) = '\0';
    return BitVectorExportBufferPtr;
}

inline unsigned char BitVector_at_bit(struct BitVector *b,size_t p) {
    BitVectorRangeCheck(b,p,"")
    return _BitVector_at_bit(b,p);
}

unsigned char BitVector_at_char(struct BitVector *b,size_t p) {
    unsigned char ans;
    for(short i; i<8; ++i,++p) {
        ans+=_BitVector_at_bit(b,p)<<i;
    }
    return ans;
}

int32_t BitVector_at_int32t(struct BitVector *b,size_t p) {
    int32_t ans;
    for(short i; i<32; ++i,++p) {
        ans+=_BitVector_at_bit(b,p)<<i;
    }
    return ans;
}

int BitVector_assign_bit(struct BitVector *b,size_t p,char t) {
    _BitVector_assign_bit(b,p,t);
    return 1;
}

int BitVector_assign_char(struct BitVector *b,size_t p,char t) {
    static const unsigned char acctor[8]={0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
    unsigned char j;
    for(short i=0; i<8; ++i) {
        j =t & acctor[i];
        _BitVector_assign_bit(b,p+i,j);
    }
    return 1;
}

int BitVector_assign_int32t(struct BitVector *b,size_t p,int32_t t) {
    unsigned char j;
    for(short i=0; i<32; ++i,++p) {
        j = (t & (0x01<<i) ) >> i;
        _BitVector_assign_bit(b,p,j);
    }
    return 1;
}

int BitVector_reverse_1bit(struct BitVector *b,size_t p) {
    b->arr[p] = (b->arr[p]) == 0 ? 1 : 0;
    return 1;
}

int BitVector_reverse_nbit(struct BitVector *b,size_t s,size_t e) {
    for(size_t i=0; i<e; ++i) {
        b->arr[s+i] = (b->arr[s+i]) == 0 ? 1 : 0;
    }
    return 1;
}

size_t BitVector_odd_amount(struct BitVector *b,size_t s,size_t e) {
    size_t ans=0;
    for(size_t i=0; i<e; ++i) {
        if(_BitVector_at_bit(b,s+i) == 1) {
            ++ans;
        }
    }
    return ans;
}

struct BitVector * BitVector_copy(struct BitVector *b) {
    struct BitVector *ans = _BitVector_init(b->len,b->gc_ctl);
    ans->len=b->len;
    if(b->arr == NULL) {
        printf("BitVector_copy::malloc ans::arr allocate error (BitVectot)%p.\n",b);
    } else {
        memcpy(ans->arr,b->arr,b->len);
    }
    return ans;
}

struct BitVector * BitVector_sub(struct BitVector *b,size_t s,size_t e) {
    struct BitVector *ans = _BitVector_init(sizeof(char)*(e+1),b->gc_ctl);
    ans->len=e;
    if(b->arr == NULL) {
        printf("BitVector_sub::malloc ans::arr allocate error (BitVectot)%p.\n",b);
    } else {
        memcpy(ans->arr,&(b->arr[s]),e);
    }
    return ans;
}

struct BitVector * BitVector_append(struct BitVector *b,struct BitVector *bv) {
    struct BitVector *ans = _BitVector_init(sizeof(char)*((b->len)+(bv->len)+1),b->gc_ctl);
    ans->len = (b->len)+(bv->len);
    if(b->arr == NULL) {
        printf("BitVector_append::malloc ans::arr allocate error (BitVectot)%p.\n",b);
    } else {
        memcpy(ans->arr,b->arr,b->len);
        memcpy(&(ans->arr[b->len]),bv->arr,bv->len);
    }
    return ans;
}


size_t BitVector_find(struct BitVector *b,struct BitVector *bv) {
    return 0;
}

int BitVector_cmp(struct BitVector *b,struct BitVector *bv) {
    if(b->len > bv->len) {
        return 1;
    } else if(b->len == bv->len) {
        return memcmp(b,bv,b->len);
    } else {
        return -1;
    }
}

struct BitVector * BitVector_insert(struct BitVector *b,struct BitVector *bv,size_t p) {
    struct BitVector *ans;
    if(p == 0) {
        ans = BitVector_append(bv,b);
    } else if(p == b->len) {
        ans = BitVector_append(b,bv);
    } else {
        char bgc_ctl=b->gc_ctl;
        b->gc_ctl=0;
        ans = BitVector_append(BitVector_sub(b,0,p),bv);
        ans = BitVector_append(ans,BitVector_sub(b,p,b->len-p));
        b->gc_ctl=bgc_ctl;
    }
    return ans;
}
// if do unit test,use unit file.
